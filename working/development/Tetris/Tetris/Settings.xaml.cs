﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Shapes;
using System.IO;
using System.Drawing;

namespace Tetris
{
    /*TETRIS
     * Michael Winkelbauer
     * 02.12.2015
     * 1.15
     * 
     * Setting from with file access
     * 
     * */
    public partial class Settings : Window, IButtons
    {
        MainWindow m = new MainWindow();
        public Settings()
        {
            InitializeComponent();
            fillBoxes();
        }
        

        public void saveButton_Click(object sender, RoutedEventArgs e)
        {
            StreamWriter sw = new StreamWriter("settings.csv");
            sw.WriteLine(difficulty.SelectedIndex.ToString()+";"+colorI.SelectedIndex.ToString()+";"+colorL.SelectedIndex.ToString()
                + ";" + colorO.SelectedIndex.ToString() + ";" + colorJ.SelectedIndex.ToString() + ";" + colorS.SelectedIndex.ToString() +
                ";" + colorT.SelectedIndex.ToString() + ";" + colorZ.SelectedIndex.ToString() +";" + background.SelectedIndex.ToString() +
                ";" + matrix.SelectedIndex.ToString());
            
            sw.Flush();
            sw.Close();
        }

        public void backButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow m = new MainWindow();
            this.Close();
            m.ShowDialog();
        }

        private void fillBoxes()
        {

            //0;red;blue;green;lila;yellow;orange;cyan;silver;silver;

            if (File.Exists("settings.csv"))
            {
                StreamReader sr = new StreamReader("settings.csv");
                string text = sr.ReadLine();
                sr.Close();
                string[] s = text.Split(';');
                switch(s[0])
                {
                    case "0":
                        difficulty.SelectedIndex = 0;
                        break;
                    case "1":
                        difficulty.SelectedIndex = 1;
                        break;
                    case "2":
                        difficulty.SelectedIndex = 2;
                        break;
                }
                colorI.SelectedIndex = returnColor(s[1]);
                colorL.SelectedIndex = returnColor(s[2]);
                colorO.SelectedIndex = returnColor(s[3]);
                colorJ.SelectedIndex = returnColor(s[4]);
                colorS.SelectedIndex = returnColor(s[5]);
                colorT.SelectedIndex = returnColor(s[6]);
                colorZ.SelectedIndex = returnColor(s[7]);
                switch (s[8])
                {
                    case "0":
                        background.SelectedIndex = 0;
                        break;
                    case "1":
                        background.SelectedIndex = 1;
                        break;
                    case "2":
                        background.SelectedIndex = 2;
                        break;
                    case "3":
                        background.SelectedIndex = 3;
                        break;
                }

                switch (s[9])
                {
                    case "0":
                        matrix.SelectedIndex = 0;
                        break;
                    case "1":
                        matrix.SelectedIndex = 1;
                        break;
                    case "2":
                        matrix.SelectedIndex = 2;
                        break;
                    case "3":
                        matrix.SelectedIndex = 3;
                        break;
                }
                

            }
            else
                MessageBox.Show("Settings not aviable");
            
        }

        private int returnColor(string number)
        {
            int selected = 0;
            switch (number)
            {
                case "0":
                    selected = 0;
                    break;
                case "1":
                    selected = 1;
                    break;
                case "2":
                    selected = 2;
                    break;
                case "3":
                    selected = 3;
                    break;
                case "4":
                    selected = 4;
                    break;
                case "5":
                    selected = 5;
                    break;
                case "6":
                    selected = 6;
                    break;
                case "7":
                    selected = 7;
                    break;
                case "8":
                    selected = 8;
                    break;
                case "9":
                    selected = 9;
                    break;
                case "10":
                    selected = 10;
                    break;
                case "11":
                    selected = 11;
                    break;
            }

            return selected;
        }

        private void dafaultButton_Click(object sender, RoutedEventArgs e)
        {
            if (File.Exists("settings.csv"))
            {
                File.Delete("settings.csv");
                StreamWriter sw = new StreamWriter("settings.csv");
                sw.WriteLine("1;11;0;8;2;1;9;10;3;3;");
                sw.Flush();
                sw.Close();
            }
                
            else
            {
                StreamWriter sw = new StreamWriter("settings.csv");
                sw.WriteLine("1;11;0;8;2;1;9;10;1;2;");
                sw.Flush();
                sw.Close();           
            }
            fillBoxes();
            
        }
    }
}
