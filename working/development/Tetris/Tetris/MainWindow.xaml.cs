﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tetris
{
    /// Tetris
    /// Michael Winkelbauer
    /// 12.12.2015
    /// 1.5
    /// Menue
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void start_Click(object sender, RoutedEventArgs e)
        {
            Game g = new Game();
            this.Close();
            g.ShowDialog();
            
        }
        private void highscore_Click(object sender, RoutedEventArgs e)
        {
            Highscore h = new Highscore();
            this.Close();
            h.ShowDialog();
        }
        private void settings_Click(object sender, RoutedEventArgs e)
        {
            Settings s = new Settings();
            this.Close();
            s.ShowDialog();
        }
        private void end_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
