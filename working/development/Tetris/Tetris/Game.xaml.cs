﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.IO;

namespace Tetris
{
    /*TETRIS
     * Michael Winkelbauer
     * 02.12.2015
     * 1.15
     * 
     * gamescreen
     * 
     * */
    public partial class Game : Window, IButtons
    {
        private float difficulty;
        public static bool shutdown = false;
        bool isset = false;
        public Game()
        {
            if (File.Exists("settings.csv"))
            {
                StreamReader sr = new StreamReader("settings.csv");
                string text = sr.ReadLine();
                sr.Close();
                string[] s = text.Split(';');

                switch (s[0])
                {
                    case "0":
                        difficulty = 0.5F;
                        game.difficulty = "Beginner";
                        break;
                    case "1":
                        difficulty = 1.25F;
                        game.difficulty = "Amateur";
                        break;
                    case "2":
                        difficulty = 1.5F;
                        game.difficulty = "Pro";
                        break;
                }


                InitializeComponent();
                switch (s[8])
                {
                    case "0":
                        Background = Brushes.Indigo;
                        break;
                    case "1":
                        Background = Brushes.Firebrick;
                        break;
                    case "2":
                        Background = Brushes.White;
                        break;
                    case "3":
                        Background = Brushes.DimGray;
                        break;
                }
            }
            else
                Background = Brushes.DimGray;

        }

        DispatcherTimer timer;
        Pitch myPitch;
       

        void Game_Initilized(object sender, EventArgs e)
        {
            timer = new DispatcherTimer();
            timer.Tick += new EventHandler(GameTick);
           
            gameStart();
        }

        private void gameStart()
        {
            MainGrid.Children.Clear();
            myPitch = new Pitch(MainGrid);
            timer.Start();
            timer.Interval = new TimeSpan(0,0,0,0,Convert.ToInt32(200/difficulty));
            shutdown = false;
        }

        void GameTick(object sender, EventArgs e)
        {
            
            game.points = myPitch.getScore();
            Score.Content = myPitch.getScore().ToString("0000");
            myPitch.currTetrominoMoveDown();
            

            if (myPitch.gameEnd()  && shutdown)
            {
                gameOver g = new gameOver();
                this.Close();
                if (!isset)
                {
                    g.ShowDialog();
                    isset = true;
                    shutdown = true;
                }
                
            }

            switch(myPitch.getScore())
            {
                case 500:
                    timer.Interval = new TimeSpan(0,0,0,0,Convert.ToInt32((170/difficulty)));
                    return;
                case 1000:
                    timer.Interval = new TimeSpan(0, 0, 0, 0, Convert.ToInt32((150 / difficulty)));
                    return;
                case 1200:
                    timer.Interval = new TimeSpan(0, 0, 0, 0, Convert.ToInt32((120 / difficulty)));
                    return;
                case 1600:
                    timer.Interval = new TimeSpan(0, 0, 0, 0, Convert.ToInt32((100 / difficulty)));
                    return;
                case 2000:
                    timer.Interval = new TimeSpan(0, 0, 0, 0, Convert.ToInt32((80 / difficulty)));
                    return;
                case 2500:
                    timer.Interval = new TimeSpan(0, 0, 0, 0, Convert.ToInt32((50 / difficulty)));
                    return;
            }
                        
        }
        private void gamePause()
        {
            if(timer.IsEnabled)
            {
                timer.Stop();
                pause.Content = "Continue!";
            }
                
            else
            {
                timer.Start();
                pause.Content = "Pause!";
            }
            
        }
        private void HandleKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Left:
                    if (timer.IsEnabled)
                        myPitch.currTetrominoMoveLeft();
                    break;
                case Key.Right:
                    if (timer.IsEnabled)
                        myPitch.currTetrominoMoveRight();
                    break;
                case Key.Down:
                    if (timer.IsEnabled)
                        myPitch.currTetrominoMoveDown();
                    break;
                case Key.Up:
                    if (timer.IsEnabled)
                        myPitch.currTetrominoMoveRotate();
                    break;
                case Key.F2:
                    gameStart();
                    break;
                case Key.F3:
                    gamePause();
                    break;
                default:
                    break;
            }
        }


        public void saveButton_Click(object sender, RoutedEventArgs e)
        {
            shutdown = false;
            isset = true;
            gameOver g = new gameOver();
            this.Close();
            g.ShowDialog();
        }

        public void backButton_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
            shutdown = true;
            MainWindow m = new MainWindow();
            this.Close();
            m.ShowDialog();
        }

        private void newGame_Click(object sender, RoutedEventArgs e)
        {
            gameStart();
        }

        private void pause_Click(object sender, RoutedEventArgs e)
        {
            gamePause();
        }
    }
}
