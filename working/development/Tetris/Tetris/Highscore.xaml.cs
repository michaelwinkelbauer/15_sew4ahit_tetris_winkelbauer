﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.OleDb;
using System.IO;

namespace Tetris
{
    /*TETRIS
     * Michael Winkelbauer
     * 02.12.2015
     * 1.15
     * 
     * Highscore form with the controlls
     * 
     * */
    public partial class Highscore : Window, IButtons
    {
        public Highscore()
        {
            Game.shutdown = false;
            InitializeComponent();
            showEntries();
            
        }

        private void showEntries()
        {
            try
            {
                if (File.Exists("highscore.accdb"))
                {
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.Connection = dbCon.getInstance();
                    cmd.CommandText = "select  p_name, difficulty from game order by points desc";
                    OleDbDataReader dr = cmd.ExecuteReader();


                    int counter = 1;
                    while (dr.Read())
                    {
                        string name = dr.GetString(0);
                        player_listbox.Items.Add(counter+".   "+dr.GetString(1)+"\t\t"+ name);
                        counter++;
                    }
                    dr.Close();
                }
                else
                    MessageBox.Show("Error while reading database! ");
            }
            catch (OleDbException oe)
            {
                MessageBox.Show(oe.ToString());
            }
        }

        public void backButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow m = new MainWindow();
            this.Close();
            m.ShowDialog();
        }

        public void saveButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow m = new MainWindow();
            this.Close();
            m.ShowDialog(); 
        }

        private void selection_Changed(object sender, SelectionChangedEventArgs e)
        {
            int points = 0;
            string date = "";
            additional_listbox.Items.Clear();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = dbCon.getInstance();
            string[] name = player_listbox.SelectedItem.ToString().Split('\t');
            cmd.CommandText = "select points, game_date from game where p_name=\""+ name[2]+"\";";
            OleDbDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                points = dr.GetInt32(0);
                date = dr.GetDateTime(1).ToString();
            }

            additional_listbox.Items.Add(points + "\t\t" + date);
        }


    }
}
