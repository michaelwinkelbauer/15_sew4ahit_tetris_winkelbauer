﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.IO;

namespace Tetris
{
    /*TETRIS
     * Michael Winkelbauer
     * 02.12.2015
     * 1.15
     * 
     * all movements are check in this class, if possible the Tetromino class will be called
     * also the current score
     * 
     * */
    class Pitch
    {
        private int rows;
        private int cols;
        private int score;
        private Tetromino currTetramino;
        public Label[,] blockControls;
        static private Brush noBrush = Brushes.Transparent;
        static private Brush matrix = Brushes.Gray;
        public Pitch(Grid tetrisGrid)
        {
            if (File.Exists("settings.csv"))
            {
                StreamReader sr = new StreamReader("settings.csv");
                string text = sr.ReadLine();
                sr.Close();
                string[] s = text.Split(';');

                switch (s[9])
                {
                    case "0":
                        matrix = Brushes.Gold;
                        break;
                    case "1":
                        matrix = Brushes.Silver;
                        break;
                    case "2":
                        matrix = Brushes.HotPink;
                        break;
                    case "3":
                        matrix = Brushes.Gray;
                        break;
                }
            }
            else
                matrix = Brushes.Gray;

            rows = tetrisGrid.RowDefinitions.Count;
            cols = tetrisGrid.ColumnDefinitions.Count;

            score = 0;
            blockControls = new Label[cols, rows];
            for (int i = 0; i < cols; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    blockControls[i, j] = new Label();
                    blockControls[i, j].Background = noBrush;
                    blockControls[i, j].BorderBrush = matrix;
                    blockControls[i, j].BorderThickness = new Thickness(1, 1, 1, 1);
                    Grid.SetRow(blockControls[i, j], j);
                    Grid.SetColumn(blockControls[i, j], i);
                    tetrisGrid.Children.Add(blockControls[i, j]);
                }
            }
            currTetramino = new Tetromino();
            currTetraminoDraw();
        }
        public int getScore()
        {
            return score;
        }
        public int getLines()
        {
            return cols;
        }
        private void currTetraminoDraw()
        {
            Point position = currTetramino.getCurrPosition();
            Point[] shape = currTetramino.getCurrShape();
            Brush color = currTetramino.getCurrColor();

            foreach (Point p in shape)
            {
                blockControls[(int)(p.X + position.X) + ((cols / 2) - 1),
                                    (int)(p.Y + position.Y)+2].Background = color;       //two additional cols, because you need these two to spawn the tetrominos
            }
        }
        private void currTetraminoDelete()
        {
            Point position = currTetramino.getCurrPosition();
            Point[] shape = currTetramino.getCurrShape();

            foreach (Point p in shape)
            {
                blockControls[(int)(p.X + position.X) + ((cols / 2) - 1),
                                    (int)(p.Y + position.Y)+2].Background = noBrush;     
            }
        }

        public void checkRows()
        {
            bool full;

            for (int i = rows - 1; i > 0; i--)
            {
                full = true;
                for (int j = 0; j < cols; j++)
                {
                    if (blockControls[j, i].Background == noBrush)
                    {
                        full = false;
                    //    if ((i == 3) & (blockControls[j, i].Background == noBrush))
                    //        MessageBox.Show("Aus");
                    }
                }
                if (full)
                {
                    removeRows(i+1);
                    score += 100;
                    checkRows();
                }
            }
        }

        private void removeRows(int row)
        {
            for (int i = row - 1; i > 2; i--)
            {
                for (int j = 0; j < cols; j++)
                {
                    blockControls[j, i].Background = blockControls[j, i - 1].Background;
                }
            }
        }

        public void currTetrominoMoveRight()
        {
            Point position = currTetramino.getCurrPosition();
            Point[] shape = currTetramino.getCurrShape();
            bool move = true;
            currTetraminoDelete();

            foreach (Point p in shape)
            {
                if (((int)(p.X + position.X) + ((cols / 2) - 1) + 1) >= cols)
                    move = false;
                else if (blockControls[((int)(p.X + position.X) + ((cols / 2) - 1) + 1),
                        (int)(p.Y + position.Y) + 2].Background != noBrush)
                    move = false;
            }
            if (move)
            {
                currTetramino.moveRight();
                currTetraminoDraw();
            }
            else
                currTetraminoDraw();
        }
        public void currTetrominoMoveLeft()
        {
            Point position = currTetramino.getCurrPosition();
            Point[] shape = currTetramino.getCurrShape();
            bool move = true;
            currTetraminoDelete();

            foreach (Point p in shape)
            {
                if (((int)(p.X + position.X) + ((cols / 2) - 1) - 1) < 0)
                    move = false;
                else if (blockControls[((int)(p.X + position.X) + ((cols / 2) - 1) - 1),
                        (int)(p.Y + position.Y) + 2].Background != noBrush)
                {
                    move = false;
                }
            }
            if (move)
            {
                currTetramino.moveLeft();
                currTetraminoDraw();
            }
            else
                currTetraminoDraw();
        }
        public void currTetrominoMoveDown()
        {
            Point position = currTetramino.getCurrPosition();
            Point[] shape = currTetramino.getCurrShape();
            bool move = true;
            currTetraminoDelete();


            foreach (Point p in shape)
            {
                if (((int)(p.Y + position.Y) + 3) >= rows)
                    move = false;
                else if (blockControls[((int)(p.X + position.X) + ((cols / 2) - 1)),
                        (int)(p.Y + position.Y) + 3].Background != noBrush)
                {
                    move = false;
                }            
            }
            if (move)
            {
                currTetramino.moveDown();
                currTetraminoDraw();
            }
            else
            {
                currTetraminoDraw();
                checkRows();
                currTetramino = new Tetromino();                         
            }

        }
        public void currTetrominoMoveRotate()
        {
            Point position = currTetramino.getCurrPosition();
            Point[] s = new Point[4];
            Point[] shape = currTetramino.getCurrShape();
            bool move = true;
            shape.CopyTo(s, 0);
            currTetraminoDelete();

            for (int i = 0; i < s.Length; i++)
            {
                double tmp = s[i].X;
                s[i].X = s[i].Y * -1;
                s[i].Y = tmp;

                if (position.X == 5)
                    move = false;
                if (((int)(s[i].Y + position.Y) + 2) >= rows)
                    move = false;
                else if (((int)(s[i].X + position.X) + ((cols / 2) - 1)) < 0)
                    move = false;
                else if (((int)(s[i].X + position.X) + ((cols / 2) - 1)) >= rows)
                    move = false;
                else if(((int)s[i].X + position.X)+3 >= 10)
                    move = false;
                //else if (blockControls[((int)(s[i].X + position.X) + ((cols / 2) - 1)), (int)(s[i].Y + position.Y)+2].Background != noBrush)
                //    move = false;
            }

            if (move)
            {
                currTetramino.moveRotate();
                currTetraminoDraw();
            }
            else
            {
                currTetraminoDraw();
            }


        }

        public bool gameEnd()
        {
            Point position = currTetramino.getCurrPosition();
            bool[] row = new bool[22];
            bool end = false;

            for (int i = 0; i < row.Length; i++)
            {
                row[i] = false;
            }
            row[0] = true;
            row[1] = true;

            for (int i = rows - 1; i >= 2; i--)
            {
                for (int j = 0; j < cols; j++)
                {
                    if (blockControls[j, i].Background != noBrush)
                    {
                        row[i] = true;
                        break;
                    }
                    if (blockControls[j, i - 1].Background != noBrush)
                    {
                        row[i - 1] = true;

                    }
                    else
                    {
                        row[i - 1] = false;
                        row[1] = true;
                    }

                }
            }
            
            for (int i = 0; i < row.Length; i++)
            {
                if (row[i] == true)
                    end = true;
                else
                {
                    end = false;
                    break;
                }

            }
            if (end)
                Game.shutdown = true;
            
            return end;
        }
    }
}
