﻿using System.Windows;

namespace Tetris
{
    /*TETRIS
     * Michael Winkelbauer
     * 02.12.2015
     * 1.15
     * 
     * Interface
     * 
     * */
    interface IButtons
    {

        void saveButton_Click(object sender, RoutedEventArgs e);

        void backButton_Click(object sender, RoutedEventArgs e);

    }
}
