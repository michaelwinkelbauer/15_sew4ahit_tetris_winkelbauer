﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.OleDb;
using System.IO;

namespace Tetris
{
    /*TETRIS
     * Michael Winkelbauer
     * 02.12.2015
     * 1.15
     * 
     * game over with database access
     * 
     * */
    public partial class gameOver : Window, IButtons
    {
        public gameOver()
        {
                Game.shutdown = false;
                InitializeComponent();
                fillForm();          
        }

        public void saveButton_Click(object sender, RoutedEventArgs e)
        {
            if(File.Exists("highscore.accdb"))
            {
                OleDbConnection con = dbCon.getInstance();
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = con;
                System.Globalization.CultureInfo.CurrentCulture.ClearCachedData();
                cmd.CommandText = "Insert into game (p_name, game_date, points, difficulty)values("+"\""+textbox.Text.ToString().PadRight(20, ' ')+"\""+", " + 
                    "\""+ DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") +"\"" + ", "+ game.points + ", \""+ game.difficulty.PadRight(10, ' ')+"\");";
                cmd.ExecuteNonQuery();


                Highscore h = new Highscore();
            
                this.Close();
                h.ShowDialog();
            }
            else
                MessageBox.Show("Database Error!");
        }

        public void backButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow m = new MainWindow();
            this.Close();
            m.ShowDialog();
        }

        private void fillForm()
        {
            l_gameOver.Content = "Game over!";
            l_points.Content = game.points;
        }
    }
}
