﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;


namespace Tetris
{
    /*TETRIS
     * Michael Winkelbauer
     * 02.12.2015
     * 1.15
     * 
     * Spawning the Tetromino and moving if possible 
     * Setting the shape and the color
     * 
     * */
    class Tetromino
    {
        private Point currPosition;
        private Point[] currShape;
        private Brush currColor;
        private bool rotateable;
        Random r = new Random();
        public Tetromino()
        {
            currPosition = new Point(0, 0);
            currColor = Brushes.Transparent;
            currShape = setRandomShape();

        }

        public Brush getCurrColor()
        {
            return currColor;
        }

        public Point getCurrPosition()
        {
            return currPosition;
        }

        public Point[] getCurrShape()
        {
            return currShape;
        }

        public void moveLeft()
        {
            currPosition.X -= 1;
        }
        public void moveRight()
        {
            currPosition.X += 1;
        }
        public void moveDown()
        {
            currPosition.Y += 1;
        }
        public void moveRotate()
        {
            if (rotateable)
            {
                for (int i = 0; i < currShape.Length; i++)
                {
                    double tmp = currShape[i].X;
                    currShape[i].X = currShape[i].Y * -1;
                    currShape[i].Y = tmp;
                }
            }
        }

        private Point[] setRandomShape()
        {
            if (File.Exists("settings.csv"))
            {
                StreamReader sr = new StreamReader("settings.csv");
                string text = sr.ReadLine();
                string[] s = text.Split(';');
                sr.Close();
                switch (r.Next() % 7)
                {
                    case 0: // I
                        rotateable = true;
                        currColor = getColor(Convert.ToInt32(s[1]));
                        return new Point[]{
                        new Point(0,0),
                        new Point(-1,0),
                        new Point(1,0),
                        new Point(2,0)
                    };
                    case 1: // J
                        rotateable = true;
                        currColor = getColor(Convert.ToInt32(s[2]));
                        return new Point[]{
                        new Point(1,1),
                        new Point(-1,0),
                        new Point(0,0),
                        new Point(1,0)
                    };
                    case 2: // L
                        rotateable = true;
                        currColor = getColor(Convert.ToInt32(s[3])); ;
                        return new Point[]{
                        new Point(0,0),
                        new Point(-1,0),
                        new Point(1,0),
                        new Point(1,-1)
                    };
                    case 3: //O
                        rotateable = false;
                        currColor = getColor(Convert.ToInt32(s[4])); ;
                        return new Point[]{
                        new Point(0,0),
                        new Point(0,1),
                        new Point(1,0),
                        new Point(1,1)
                    };
                    case 4: // S
                        rotateable = true;
                        currColor = getColor(Convert.ToInt32(s[5])); ;
                        return new Point[]{
                        new Point(0,0),
                        new Point(-1,0),
                        new Point(0,-1),
                        new Point(1,0)
                    };
                    case 5: // T
                        rotateable = true;
                        currColor = getColor(Convert.ToInt32(s[6])); ;
                        return new Point[]{
                        new Point(0,0),
                        new Point(1,0),
                        new Point(0,-1),
                        new Point(1,1)
                    };
                    case 6: // Z
                        rotateable = true;
                        currColor = getColor(Convert.ToInt32(s[7])); ;
                        return new Point[]{
                        new Point(0,0),
                        new Point(-1,0),
                        new Point(0,1),
                        new Point(1,1)
                    };

                    default:
                        return null;
                }
            }
            else
            {
                StreamWriter sw = new StreamWriter("settings.csv");
                sw.WriteLine("1;11;0;8;2;1;9;10;1;2;");
                sw.Flush();
                sw.Close();
                setRandomShape();
                return null;
            }
        }

        private Brush getColor(int id)
        {
            Brush b = Brushes.Blue;
            switch (id)
            {
                case 0:
                    b = Brushes.Blue;
                    break;
                case 1:
                    b = Brushes.Green;
                    break;
                case 2:
                    b = Brushes.Yellow;
                    break;
                case 3:
                    b = Brushes.AliceBlue;
                    break;
                case 4:
                    b = Brushes.Black;
                    break;
                case 5:
                    b = Brushes.DeepPink;
                    break;
                case 6:
                    b = Brushes.Lavender;
                    break;
                case 7:
                    b = Brushes.Olive;
                    break;
                case 8:
                    b = Brushes.Orange;
                    break;
                case 9:
                    b = Brushes.Purple;
                    break;
                case 10:
                    b = Brushes.Red;
                    break;
                case 11:
                    b = Brushes.Cyan;
                    break;
            }
            return b;

        }
    }
}
