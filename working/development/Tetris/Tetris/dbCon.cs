﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace Tetris
{
    /*TETRIS
     * Michael Winkelbauer
     * 02.12.2015
     * 1.15
     * 
     * database connection singleton
     * 
     * */
    class dbCon
    {

     private static OleDbConnection conInstance = null;

        private dbCon(string conStr)
        {
            conInstance = new OleDbConnection(conStr);
            try
            {
                conInstance.Open();
            }
            catch (OleDbException e)
            {
                Console.WriteLine(e.Message.ToString());
            }

        }
        public static OleDbConnection getInstance()
        {
            string executable = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string path = (System.IO.Path.GetDirectoryName(executable));
            AppDomain.CurrentDomain.SetData("DataDirectory", path);

            if (conInstance == null)
            {
                dbCon  conDB = new dbCon(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\highscore.accdb;Persist Security Info=False;");
                return conInstance;
            }
            else
                return conInstance;
        }
        public static void CloseCon()
        {
            conInstance.Close();
        }

    }
}
